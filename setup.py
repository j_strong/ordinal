try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
        'description': 'a function to get the ordinal string of an integer.',
        'author': 'Jonathan Strong',
        'url': 'https://j_strong@bitbucket.org/j_strong/ordinal.git',
        'author_email': 'jonathan.strong@gmail.com',
        'version': '0.1',
        'install_requires': [],
        'packages': ['ordinal'],
        'scripts': [],
        'name': 'ordinal',
}
setup(**config)
